### pre run build tasks
```bash
# download @privacyblockchain/core-client to get the AbiUtils

# need to manually download and build partisia crypto because it depends on AbiUtils
git clone https://gitlab.com/partisiaapplications/partisia-crypto.git
cd partisia-crypto
npm i
npm run build
```

### To run airdrop
```bash
# install dependencies
npm i

# build
npm run build

# copy and adjust your config as needed
cp example.env .env

# run the airdrop
node lib/index.js
```
