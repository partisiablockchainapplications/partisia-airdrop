require('dotenv').config()

import addresses from "./addresses.json";
import { partisiaCrypto } from "partisia-crypto";
import { PartisiaRpc } from "partisia-rpc";
import { PartisiaRpcClass } from "partisia-rpc/lib/main/rpc";
import assert from "assert";
import axios from "axios";

const sleep = (ms: number) => {
    return new Promise((resolve) => setTimeout(resolve, ms))
}
const broadcastTransactionPoller = async (trxHash: string, rpc: PartisiaRpcClass, num_iter = 10, interval_sleep = 2000) => {
    let intCounter = 0
    while (++intCounter < num_iter) {
        try {
            console.log('intCounter', intCounter)
            const resTx = await rpc.getTransaction(trxHash)
            if (resTx.finalized) {
                break
            }
        } catch (error: any) {
            console.error(error.message)
        } finally {
            await sleep(interval_sleep)
        }
        return intCounter < num_iter
    }
}

// main entry point
(async () => {
    const aryAddresses = addresses.filter((val, idx, ary) => ary.indexOf(val) === idx).sort()
    console.log('addresses count', aryAddresses.length)


    // Send 100 tokens to each
    const wif = process.env.wif as string
    const abiHex = process.env.abiHex as string
    const contract = process.env.contract as string

    const rpc = PartisiaRpc({ baseURL: process.env.baseUrl as string })


    // deserialize the abi bytes into abiFile
    const abiFile = partisiaCrypto.AbiUtils.deserializeAbi(Buffer.from(abiHex, 'hex'))
    const action: any = abiFile.abi.actions.find(a => a.name === 'transfer')

    // loop until all address have a balance
    while (true) {
        let nonce = await rpc.getNonce(process.env.address as string)
        // get the current balances
        const { data } = await axios.get(`https://betareader.partisiablockchain.com/shards/Shard1/blockchain/contracts/${contract}?requireContractState=true`)
        const state = data.serializedContract.state.data
        const deserializeState: any = partisiaCrypto.AbiUtils.deserializeState(abiFile.abi, Buffer.from(state, 'base64'))
        const balances = Object.keys(deserializeState.balances).sort()
        console.log(`total previously airdropped: ${balances.length}`)

        for (const address of aryAddresses) {
            try {
                if (balances.includes(address)) continue

                const args = [address, '1000000']
                const bufPayload = partisiaCrypto.AbiUtils.serializeAction(action, args, abiFile.abi)

                const dataInner = {
                    nonce: nonce++,
                    cost: 6000
                }
                const dataHeader = {
                    contract
                }
                const serializedTransaction = partisiaCrypto.transaction.serializedTransaction(dataInner, dataHeader, bufPayload)
                const digest = partisiaCrypto.transaction.deriveDigest('Partisia Blockchain (RC.1)', serializedTransaction)


                const sig = partisiaCrypto.wallet.signTransaction(digest, wif)
                const trxHash = partisiaCrypto.transaction.getTrxHash(digest, sig)

                const trxPayload = partisiaCrypto.transaction.getTransactionPayloadData(serializedTransaction, sig)
                const res = await rpc.broadcastTransaction(trxPayload)
                assert(res, 'unable to broadcast on chain')
                const check = await broadcastTransactionPoller(trxHash, rpc)
                assert(check, 'unable to finalize on chain')

                console.log(`address: ${address} trx_hash: ${trxHash}`)
            } catch (error: any) {
                console.error(`${address}: ${error.message}`)

                break
            }
        }
        if (balances.length === aryAddresses.length) break
    }

})()

